package com.devcamp.s50.task5850.restapi.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.s50.task5850.restapi.model.CEmployees;
import com.devcamp.s50.task5850.restapi.repository.IEmployeeRepo;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CEmployeeController {
    @Autowired
    IEmployeeRepo iEmployeeRepo;

    @GetMapping("/employees")
    public ResponseEntity<List<CEmployees>> getEmployeeList() {
        List<CEmployees> employeeList = new ArrayList<>();
        try {
            iEmployeeRepo.findAll().forEach(employeeList::add); 
            return new ResponseEntity<>(employeeList,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
}
}