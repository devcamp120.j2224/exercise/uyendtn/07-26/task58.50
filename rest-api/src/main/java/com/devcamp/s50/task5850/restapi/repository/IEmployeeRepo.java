package com.devcamp.s50.task5850.restapi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.s50.task5850.restapi.model.CEmployees;

public interface IEmployeeRepo extends JpaRepository<CEmployees,Long> {
    
}
